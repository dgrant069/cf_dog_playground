function Dog(name, age, breed){
  this.name = name;
  this.age = age;
  this.breed = breed;

}

Dog.prototype.toString = function(){
  return "\nYour dog's name is: " + this.name + ". It's " + this.age +
         " years old and belongs to the breed " + this.breed;
};


var izze = new Dog("Izze", "6", "Boston Terrier");
var tucker = new Dog("Tucker", "13", "Golden Retriever");
var allDogs = [izze, tucker]

console.log(allDogs.toString());

// function DogLog (dog){
//   var allDogs = [izze, tucker];
//   var result =[];
//   for (var i in allDogs){
//     result.push(i)
//   }
//   result.toString();
// }

// console.log(DogLog.result);
